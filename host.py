import socket, random, time

hosts = ['54.172.36.162', '54.242.196.78', '54.197.0.240']
dead_hosts = []

PORT = 15001

s = socket.socket()
while True:
    HOST = random.choice(hosts)
    res = s.connect_ex((HOST,PORT))
    if res != 0:
        dead_hosts.append(HOST)
        print("Server: {} is not active".format(HOST))
        if all(item in dead_hosts for item in hosts):
            print("There is no active server at the moment")
            break
    else:
        print("Server: {} is active".format(HOST))
        print("----------------------------------")
        break

if not all(item in dead_hosts for item in hosts):
    boolean = True
    while boolean:
        # message sent to server
        input_string = input("Enter data you want to send: ")
        start = time.perf_counter()
        s.sendall(bytes(input_string,'utf-8'))

        # print the received message
        print(s.recv(1024).decode())
        print(s.recv(1024).decode())
        print("Public IP server: {}".format(HOST))

        result = round(time.perf_counter()-start, 3)
        print("\nTime taken :", result ,"seconds")

        if(result >= 60.0):
            print("Type of task: Long task")
        elif (result >= 1 and result < 60.0):
            print("Type of task: Short task")
        else:
            print("Type of task: Very short task")

        # ask to input another
        ans = input('\nDo you want to continue(y/n) :')
        if ans == 'y':
            continue
        else:
            boolean = False
    s.close()
