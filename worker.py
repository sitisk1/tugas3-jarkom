import socket

HOST = '0.0.0.0'
PORT = 15001

s = socket.socket()
print("Socket successfully created")
s.bind((HOST, PORT))
print("Socket binded to",PORT)
s.listen()
print("Socket is listening")

myHostName = socket.gethostbyname(socket.gethostname())

while True:
    try:
        c, addr = s.accept()
        print('Got connection from', addr)

        # Get data from client
        while True:
            data = c.recv(1024).decode()
            if not data:
                break

            c.sendall(bytes("Task is running",'utf-8'))

            #Reverse data received from client
            data = data[::-1]
            c.sendall(bytes("Task is finished",'utf-8'))

            # Send back reversed data to client
            c.sendall(bytes("\nReverse of data: {}".format(data),'utf-8'))
            c.sendall(bytes("\n\nPrivate IP server: {}".format(myHostName), 'utf-8')) 

    except socket.error as msg:
        c.sendall(bytes("\nTask failed. Error Code:: {}. Message: {}".format(str(msg[0]), msg[1]),'utf-8'))

    # Close the connection with the client
    c.close()

c.close()
