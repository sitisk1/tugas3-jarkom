# Tugas 3 - Jaringan Komputer

Anggota Kelompok G2:
  - Inez Amandha Suci - 1806141233
  - Nabila Ayu Dewanti - 1806205312
  - Siti Khadijah - 1806205331

### Penjelasan

- **host.py**
Pada program host.py, program ini akan digunakan oleh client atau host. Terdapat satu client yang digunakan PC/Laptopnya untuk dijadikan sebagai master node.
Secara general, host.py ini berfungsi mengkoneksikan client dengan server. Selain itu, mencari server manakah yang aktif sehingga client dapat berhubungan dengan server tersebut untuk mengirimkan suatu data. Jika sudah ditemukan server yang aktif, maka client akan selalu mengirimkan data kepada server tersebut dengan melalui input. Pada host.py, juga terdapat perhitungan berapa lama worker akan bekerja.
Jika sudah selesai, maka client menerima hasil olahan data dari worker.py. Lalu, host.py juga yang bertugas mengatur apakah client ingin melanjutkan job atau tidaknya.

- **worker.py**
Pada program worker.py, program ini akan digunakan oleh server atau worker. Bisa terdapat banyak worker yang mana artinya terdapat banyak server yang sedang aktif dan turut menjalankan program ini. Nantinya, host akan memilih yang manakah yang dibacanya bahwa server tersebut aktif dan memilihnya secara random.
Secara general, worker.py ini berfungsi untuk mengolah data yang diterimanya dari client dan mengembalikannya pada client. Pada program yang kelompok G2 buat, worker.py berfungsi untuk mengolah data menjadi kebalikan katanya lalu mengembalikannya kepada client. Selain itu, diberikan juga beberapa error catching bila terdapat kesalahan. Terdapat pula pemberitahuan bahwa task sedang berjalan dan sudah selesai.

Adapun untuk penjelasan per fitur atau penjelasan yang lebih lengkap dari masing-masing program ini, dapat diakses dengan mengklik link berikut:
[Laporan Tugas 3 - G2]

[Laporan Tugas 3 - G2]: <https://docs.google.com/document/d/1Gg6lbGo6GvNkS3l8OGqL63VfC-NlMCmGAAlKSSspfwQ/edit?usp=sharing>